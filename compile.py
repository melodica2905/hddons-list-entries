#!/usr/bin/env python3

import os
import re
import sys
import json
import markdown
import mimetypes
from markdown.preprocessors import Preprocessor
from markdown.treeprocessors import Treeprocessor
from markdown.treeprocessors import etree
from markdown.extensions import Extension
from ruamel.yaml import YAML
from datetime import datetime

class DropdownPreprocessor(Preprocessor):
	D1_RE = re.compile(r".*\n<<<", re.MULTILINE)
	D2_RE = re.compile(r"<<<", re.MULTILINE)

	def run(self, lines):
		text = "\n".join(lines)
		start = True
		while 1:
			if start:
				m = self.D1_RE.search(text)
			else:
				m = self.D2_RE.search(text)

			if m:
				if start:
					content = m.group().split("\n")
					tmp = self.md.htmlStash.store(
						"<details>\n<summary>{}</summary>".format(content[0])
					)
					start = False
				else:
					tmp = self.md.htmlStash.store("</details>")
					start = True

				text = "{}\n{}\n{}".format(
					text[:m.start()],
					tmp,
					text[m.end():]
				)

			else:
				break

		return text.split("\n")

class IconLinkPreprocessor(Preprocessor):
	ICONLINK_RE = re.compile(r".+\|.+\|.+", re.MULTILINE)

	def run(self, lines):
		text = "\n".join(lines)
		while 1:
			m = self.ICONLINK_RE.search(text)

			if m:
				content = m.group().split("|")
				tmp = self.md.htmlStash.store(
					"<div class=iconlink><a target=_blank href={href}><img src={img}>{text}</a></div>".format(
						href = content[2],
						img = content[1],
						text = content[0]
					)
				)
				text = "{}\n{}\n{}".format(
					text[:m.start()],
					tmp,
					text[m.end():]
				)

			else:
				break

		return text.split("\n")

class InlineEmbedVideoProcessor(Treeprocessor):
	def run(self, root):
		for element in root.iter('img'):
			if element.attrib['src'].find("https://youtu") != -1 or element.attrib['src'].find("https://www.youtu") != -1:
				element.tag = 'iframe'
				if element.attrib['src'].find("youtu.be") != -1:
					element.attrib['src'] = element.attrib['src'].replace("youtu.be/", "youtube.com/embed/")

				else:
					element.attrib['src'] = element.attrib['src'].replace("watch?v=", "embed/")

			elif element.attrib['src'].find("https://streamable.com") != -1:
				element.tag = 'iframe'
				element.attrib['src'] = element.attrib['src'].replace(".com/", ".com/e/")

			else:
				# check if mimetype is valid
				urltocheck = element.attrib['src'].split('?')[0]
				mimetype,encoding = mimetypes.guess_type(urltocheck)
				if mimetype == None:
					element.tag = 'iframe' # honestly, idk how to properly handle this, so i'll just let the browser resolve this by itself :]

class InlineLazyLoadProcessor(Treeprocessor):
	def run(self, root):
		for element in root.iter('img'):
			element.attrib['loading'] = 'lazy'

		for element in root.iter('iframe'):
			element.attrib['loading'] = 'lazy'

		for element in root.iter('video'):
			element.attrib['preload'] = 'none'

class InlineTargetBlankProcessor(Treeprocessor):
	def run(self, root):
		for element in root.iter('a'):
			element.attrib['target'] = '_blank'

class Dastension(Extension):
	def extendMarkdown(self, md):
		md.preprocessors.register(IconLinkPreprocessor(md), "iconlink", 25)
		md.preprocessors.register(DropdownPreprocessor(md), "dropdown", 25)
		# default inlineprocessor uses 20
		md.treeprocessors.register(InlineEmbedVideoProcessor(md), "embedvideo", 15)
		md.treeprocessors.register(InlineLazyLoadProcessor(md), "lazyload", 15)
		md.treeprocessors.register(InlineTargetBlankProcessor(md), "targetblank", 15)

def processMarkdown(text):
	return markdown.markdown(
		text,
		output_format="html5",
		extensions=[
			'nl2br',
			'fenced_code',
			'pymdownx.tilde',
			'pymarkdown-video',
			Dastension()
		]
	)

# Main
directory = ""

# Nothing entered / Not enough arguments
if len(sys.argv) < 2:
	print("Usage:")
	print("compile.py <category>")
	print("")
	print("Outputs to ./list_output/")
	exit()

# Too many arguments
if len(sys.argv) > 2:
	print("You provided more than 1 directory. Don't do that. Ignoring the other directories.")

subdirectory = sys.argv[1]
files = []
output_path = ""

try:
	os.mkdir("list_output")
except OSError as error:
	print(error)

output_path = os.path.join("list_output", subdirectory + "_list.json")

# Get files
files = [os.path.join(subdirectory, file) for file in os.listdir(subdirectory) if os.path.isfile(os.path.join(subdirectory, file)) and file != "about.txt"]
files.sort()
print(str(files))

# Get files contents

date = datetime.now().strftime("%d/%m/%Y")
result = '{"last_updated":"' + date + '",'

# Get the About file
with open(os.path.join(subdirectory, "about.txt"), "r") as file:
	print("Reading about.txt")
	result += json.dumps({"about":processMarkdown(file.read())})[1:-1] + ","

entries = []
entry_headers = []

result += '"list":['
for f in files:
	print("Reading " + f)

	with open(f, "r") as file:
		contents = YAML(typ="safe").load(file.read())

		# Translate Markdown
		contents["description"] = processMarkdown(contents["description"])
		if "dependencies" in contents.keys():
			for i in range(0, len(contents["dependencies"])):
				contents["dependencies"][i] = processMarkdown(contents["dependencies"][i])

		if "flairs" in contents.keys():
			flairs = contents["flairs"]
			contents["flairs"] = {}
			for i in flairs:
				contents["flairs"][processMarkdown(i)] = flairs[i]
			print(contents["flairs"])

		entries.append(contents)
		entry_headers.append("{}|{}".format(
			contents["title"],
			contents["credits"]
		))

entry_headers.sort()

first = True

# Store as json
for head in entry_headers:
	tnc = head.split("|")
	for entry in entries:
		if not (entry["title"] == tnc[0] and entry["credits"] == tnc[1]):
			continue

		if not first:
			result += ','

		result += json.dumps(entry)
		first = False
		break

result += ']}'

# Wait a minute... Isn't this just JSON format?
# Yes. Now export it for the website to read :]
with open(output_path, "w") as l:
	l.write(result)
	print("Wrote to " + output_path)
